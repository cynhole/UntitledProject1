local Tag = "BuildLayers"
local NET_BL_GETLAYERS = Tag .. "_GetLayers"

BuildLayers = BuildLayers or {
    Overrides = {},
    Layers = {
        ["main"] = {
            owner = "world",
            desc = "The normal layer as if it were any other server.",
            public = true,
        },
        ["public1"] = {
            owner = "world",
            desc = "A public layer for when main feels too crowded for you.",
            public = true,
        },
        ["public2"] = {
            owner = "world",
            desc = "A public layer for when main feels too crowded for you.",
            public = true,
        },
        ["public3"] = {
            owner = "world",
            desc = "A public layer for when main feels too crowded for you.",
            public = true,
        },
        ["public4"] = {
            owner = "world",
            desc = "A public layer for when main feels too crowded for you.",
            public = true,
        },
        ["trusted+"] = {
            owner = "world",
            desc = "Layer for trusted rank and above.",
            public = false,
            CustomPermsCheck = function(ply)
                return ply:CheckUserGroupLevel("trusted")
            end
        }
    },
    LayerPerms = {}
}

local PLAYER = FindMetaTable("Player")
local ENTITY = FindMetaTable("Entity")

function ENTITY:GetBuildLayer()
    return self:GetNW2String(Tag,"main")
end

if SERVER then
    function BuildLayers.AddLayer(name,owner,desc,public)
        if not IsValid(owner) then return end
        BuildLayers.Layers[name] = {
            owner = owner,
            desc = desc or "No description given.",
            public = public or false
        }

        MsgC(Color(0,255,255),"[BuildLayers] ") Msg(tostring(owner)) print((" Created layer \"%s\""):format(name))
    end

    function ENTITY:SetBuildLayer(layer)
        local oldLayer = self:GetBuildLayer()
        self:SetCustomCollisionCheck(true)
        self:CollisionRulesChanged()
        self:SetNW2String(Tag, layer)

        if oldLayer ~= layer and self:IsPlayer() then
            MsgC(Color(0,255,255),"[BuildLayers] ") Msg(tostring(self)) print((" %s -> %s"):format(oldLayer, layer))
        end
    end

    for _,ent in pairs(ents.GetAll()) do
        if ent:MapCreationID() ~= -1 then
            ent:SetNW2Bool("Layers_IsWorldEntity", true)
        end
    end

    -- START OVERRIDES --
    BuildLayers.Overrides.PlayerGive = BuildLayers.Overrides.PlayerGive or PLAYER.Give
    function PLAYER:Give(class, noammo)
        noammo = noammo or false
        local wep = ents.Create(class)
        wep:SetPos(self:GetPos())
        wep:Spawn()
        wep:SetBuildLayer(self:GetBuildLayer())

        return wep
    end

    BuildLayers.Overrides.CreateKeyframeRope = BuildLayers.Overrides.CreateKeyframeRope or constraint.CreateKeyframeRope
    local kfEntCache = game.GetWorld()
    function constraint.CreateKeyframeRope(pos, width, material, constr, ent1, lpos1, bone1, ent2, lpos2, bone2, kv)
        local rope = BuildLayers.Overrides.CreateKeyframeRope(pos, width, material, constr, ent1, lpos1, bone1, ent2, lpos2, bone2, kv)

        if rope then
            if ent1:IsWorld() and not ent2:IsWorld() then
                rope:SetNW2Entity("LayerRopeEnt",ent2)
            elseif not ent1:IsWorld() and ent2:IsWorld() then
                rope:SetNW2Entity("LayerRopeEnt",ent1)
            else
                rope:SetNW2Entity("LayerRopeEnt",kfEntCache)
            end
        end

        kfEntCache = ent1

        return rope
    end
    -- END OVERRIDES --

    local function SetLayerOnSpawn(ply,ent)
        ent:SetBuildLayer(ply:GetBuildLayer())
        if table.Count(ent:GetChildren()) > 0 then
            for _, child in pairs(ent:GetChildren()) do
                child:SetBuildLayer(ply:GetBuildLayer())
            end
        end
        if ent.ClassName and ent.ClassName == "gmod_sent_vehicle_fphysics_base" then
            timer.Simple(1,function()
                if ent.Wheels then
                    for _, child in pairs(ent.Wheels) do
                        child:SetBuildLayer(ply:GetBuildLayer())
                    end
                end
            end)
        end
    end
    local function SetLayerOnSpawnMdl(ply,model,ent)
        SetLayerOnSpawn(ply,ent)
    end
    local function TwoEntCheck(ent1,ent2)
        if not IsValid(ent1) or not IsValid(ent2) then return end
        return ent1:GetBuildLayer() == ent2:GetBuildLayer()
    end
    local function CrossLayerMove(ent1,ent2)
        if not IsValid(ent1) or not IsValid(ent2) then return end
        if ent1:GetBuildLayer() ~= ent2:GetBuildLayer() then
            ent2:SetBuildLayer(ent1:GetBuildLayer())
        end
    end

    hook.Add("PlayerSpawnedEffect", Tag, SetLayerOnSpawnMdl)
    hook.Add("PlayerSpawnedNPC", Tag, SetLayerOnSpawn)
    hook.Add("PlayerSpawnedProp", Tag, SetLayerOnSpawnMdl)
    hook.Add("PlayerSpawnedRagdoll", Tag, SetLayerOnSpawnMdl)
    hook.Add("PlayerSpawnedSENT", Tag, SetLayerOnSpawn)
    hook.Add("PlayerSpawnedVehicle", Tag, SetLayerOnSpawn)
    hook.Add("PlayerSpawnedSWEP", Tag, SetLayerOnSpawn)

    hook.Add("AllowPlayerPickup", Tag, TwoEntCheck)
    hook.Add("GravGunPickupAllowed", Tag, TwoEntCheck)
    hook.Add("PlayerCanPickupItem", Tag, TwoEntCheck)
    hook.Add("PlayerCanPickupWeapon", Tag, TwoEntCheck)
    hook.Add("PlayerCanHearPlayersVoice", Tag, TwoEntCheck)
    hook.Add("CanPlayerEnterVehicle", Tag, TwoEntCheck)

    hook.Add("PhysgunDrop", Tag, CrossLayerMove)
    hook.Add("GravGunOnDropped", Tag, CrossLayerMove)
    hook.Add("PlayerEnteredVehicle", Tag, CrossLayerMove)

    hook.Add("PlayerInitialSpawn", Tag, function(ply)
        ply:SetBuildLayer("main")
    end)

    hook.Add("PlayerShouldTakeDamage", Tag, function(vic,att)
        if not IsValid(vic) or not IsValid(att) then return end

        if vic:GetBuildLayer() ~= att:GetBuildLayer() then
            return false
        end
    end)

    hook.Add("PlayerDisconnected", Tag, function(ply)
        for name, data in next, BuildLayers.Layers do
            if data.owner == ply then
                BuildLayers.Layers[name] = nil
                for _, pl in next, player.GetAll() do
                    if pl:GetBuildLayer() == name then
                        pl:SetBuildLayer("main")
                        pl:ChatPrint(("BuildLayers: You were moved out of layer \"%s\" because the owner has left."):format(name))
                    end
                end
            end
        end
    end)

    hook.Add("CreateEntityRagdoll", Tag, function(owner, ragdoll)
        if not IsValid(owner) or not IsValid(ragdoll) then return end
        ragdoll:SetBuildLayer(owner:GetBuildLayer())
    end)

    hook.Add("OnViewModelChanged", Tag, function(vm, old, new)
        if not IsValid(vm) or not IsValid(vm:GetOwner()) then return end
        vm:SetBuildLayer(vm:GetOwner():GetBuildLayer())
    end)

    local LayerNameBlacklist = {
        add = true,
        delete = true,
        allow = true,
        deny = true,
        open = true,
        close = true,
        create = true,
    }

    if aowl then
        aowl.AddCommand("layer", function(ply, line, layer, name, desc, public)
            if layer == "add" or layer == "create" then
                name = name:lower()
                public = tobool(public)

                if BuildLayers.Layers[name] then
                    return false, "Layer already exists with that name."
                end
                if LayerNameBlacklist[name] then
                    return false, "Blacklisted layer name."
                end

                BuildLayers.AddLayer(name, ply, desc, public)
                ply:ChatPrint(("BuildLayers: %sayer %s created."):format(public and "L" or "Private l", name))

                if not public then
                    BuildLayers.LayerPerms[name] = {}
                end
            elseif layer == "delete" then
                if not BuildLayers.Layers[name] then
                    return false, "Layer doesn't exist."
                end

                if BuildLayers.Layers[name].owner ~= ply then
                    return false, "You are not the owner."
                end

                BuildLayers.Layers[name] = nil
                for _, pl in next, player.GetAll() do
                    if pl:GetBuildLayer() == name then
                        pl:SetBuildLayer("main")
                        pl:ChatPrint(("BuildLayers: You were moved out of layer \"%s\" because the layer has been deleted."):format(name))
                    end
                end

                ply:ChatPrint(("BuildLayers: Deleted layer \"%s\"."):format(name))
            elseif layer == "allow" then
                if not BuildLayers.Layers[name] then
                    return false, "Layer doesn't exist."
                end

                if BuildLayers.Layers[name].owner ~= ply then
                    return false, "You are not the owner."
                end

                if not BuildLayers.LayerPerms[name] then
                    BuildLayers.LayerPerms[name] = {}
                end

                local pl = aowl.StringToType("player", desc, ply)
                if not pl or not IsValid(pl) or not pl:IsPlayer() then
                    return false, aowl.TargetNotFound(desc)
                end
                if ply == pl then
                    return false, "No need to allow yourself."
                end
                if BuildLayers.LayerPerms[name][pl] then
                    return false, "Player already allowed."
                end

                BuildLayers.LayerPerms[name][pl] = true
                ply:ChatPrint(("BuildLayers: Allowed %s from %s."):format(pl:Name(), name))
            elseif layer == "deny" then
                if not BuildLayers.Layers[name] then
                    return false, "Layer doesn't exist."
                end

                if BuildLayers.Layers[name].owner ~= ply then
                    return false, "You are not the owner."
                end

                if not BuildLayers.LayerPerms[name] then
                    BuildLayers.LayerPerms[name] = {}
                end

                local pl = aowl.StringToType("player", desc, ply)
                if not pl or not IsValid(pl) or not pl:IsPlayer() then
                    return false, aowl.TargetNotFound(desc)
                end
                if ply == pl then
                    return false, "No need to deny yourself."
                end
                if not BuildLayers.LayerPerms[name][pl] then
                    return false, "Player already denied."
                end

                BuildLayers.LayerPerms[name][pl] = nil
                ply:ChatPrint(("BuildLayers: Denied %s from %s."):format(pl:Name(), name))

                if pl:GetBuildLayer() == name then
                    pl:SetBuildLayer("main")
                    pl:ChatPrint(("BuildLayers: You were moved out of layer \"%s\" because the owner has denied you access."):format(name))
                end
            elseif layer == "open" then
                if not BuildLayers.Layers[name] then
                    return false, "Layer doesn't exist."
                end

                if BuildLayers.Layers[name].owner ~= ply then
                    return false, "You are not the owner."
                end

                if BuildLayers.Layers[name].public then
                    return false, "Already public."
                end

                BuildLayers.Layers[name].public = true
                ply:ChatPrint(("BuildLayers: Made layer \"%s\" public."):format(name))
            elseif layer == "close" then
                if not BuildLayers.Layers[name] then
                    return false, "Layer doesn't exist."
                end

                if BuildLayers.Layers[name].owner ~= ply then
                    return false, "You are not the owner."
                end

                if not BuildLayers.Layers[name].public then
                    return false, "Already private."
                end

                if not BuildLayers.LayerPerms[name] then
                    BuildLayers.LayerPerms[name] = {}
                end

                for _, pl in next, player.GetAll() do
                    if pl == ply then continue end
                    if pl:GetBuildLayer() == name then
                        BuildLayers.LayerPerms[name][pl] = true
                    end
                end

                BuildLayers.Layers[name].public = false
                ply:ChatPrint(("BuildLayers: Made layer \"%s\" private and whitelisted current players."):format(name))
            else
                if not BuildLayers.Layers[layer] then
                    return false, "Layer doesn't exist."
                end

                if
                BuildLayers.Layers[layer].public
                or (not BuildLayers.Layers[layer].public and BuildLayers.LayerPerms[layer] and BuildLayers.LayerPerms[layer][ply])
                or (not BuildLayers.Layers[layer].public and BuildLayers.Layers[layer].owner == ply)
                or (not BuildLayers.Layers[layer].public and BuildLayers.Layers[layer].CustomPermsCheck and BuildLayers.Layers[layer].CustomPermsCheck(ply)) then
                    ply:SetBuildLayer(layer)
                    ply:ChatPrint(("BuildLayers: You are now in layer \"%s\"."):format(layer))
                else
                    return false, ("Not allowed to join layer \"%s\"."):format(layer)
                end
            end
        end)
    end
end

if CLIENT then
    local function SetLayerVisibility(ent, layer)
        if ent:EntIndex() <= 0 or not IsValid(ent) then return end

        local visible = false

        if IsValid(ent:GetOwner()) then
            visible = ent:GetOwner():GetBuildLayer() == layer
        elseif ent:GetClass() == "class C_RopeKeyframe" then
            visible = ent:GetNW2Entity("LayerRopeEnt", ent):GetBuildLayer() == layer
        else
            visible = ent:GetBuildLayer() == layer
        end

        if ent:GetClass() == "class C_RopeKeyframe" then
            if visible then
                ent:SetColor(255, 255, 255, 255)
            else
                ent:SetColor(255, 255, 255, 0)
            end
        else
            ent:SetNoDraw(not visible)

            if visible and not ent.LayerVisibility then
                ent:CreateShadow()
            end
        end

        ent.LayerVisibility = visible
    end

    hook.Add("RenderScene", Tag, function()
        local layer = LocalPlayer():GetBuildLayer()

        for _, ent in ipairs( ents.GetAll() ) do
            if ent:GetNW2Bool("Layers_IsWorldEntity", false) == true then continue end
            SetLayerVisibility(ent,layer)

            if ent.Layer and ent.Layer ~= ent:GetBuildLayer()
                and (ent.Layer ~= layer and ent:GetBuildLayer() == layer)
                or (ent.Layer == layer and ent:GetBuildLayer() ~= layer)
            then
                local effect = EffectData()
                effect:SetEntity(ent)
                util.Effect("entity_remove", effect, true, true)
            end

            ent.Layer = ent:GetBuildLayer()
        end
    end)

    hook.Add("CreateClientsideRagdoll", Tag, function(owner, ragdoll)
        if not IsValid(owner) or not IsValid(ragdoll) then return end
        ragdoll.Layer = owner:GetBuildLayer()
    end)
end

hook.Add("ShouldCollide",Tag,function(ent1,ent2)
    if not IsValid(ent1) or not IsValid(ent2) then return true end
    if SERVER and (ent1:MapCreationID() ~= -1 or ent2:MapCreationID() ~= -1) or (ent1:GetNW2Bool("Layers_IsWorldEntity",false) == true or ent2:GetNW2Bool("Layers_IsWorldEntity",false) == true) then
        return true --world ents
    end

    return ent1:GetBuildLayer() == ent2:GetBuildLayer()
end)

hook.Add("PhysgunPickup",Tag,function(ply,ent)
    if not IsValid(ply) or not IsValid(ent) then return end
    return ply:GetBuildLayer() == ent:GetBuildLayer()
end)

hook.Add("GravGunPunt", Tag, function(ply, ent)
    if not IsValid(ent1) or not IsValid(ent2) then return end
    return ent1:GetBuildLayer() == ent2:GetBuildLayer()
end)

function ents.FindInLayer(layer)
    local out = {}

    for _, ent in pairs(ents.GetAll()) do
        if not IsValid(ent) or ent:GetNW2Bool("Layers_IsWorldEntity", false) == true or ent:EntIndex() <= 0 then continue end
        if ent:GetBuildLayer() == layer then
            out[#out + 1] = ent
        end
    end

    return out
end