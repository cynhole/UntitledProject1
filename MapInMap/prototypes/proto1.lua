-- This prototype was the BSP reading approach.
-- Inspired by MapPatcher's BSP library and gmDoom.

local luabsp = MapPatcher.Libs.luabsp

local map = luabsp.LoadMap("cs_office")

-- util funcs from luabsp itself, original only gave clip brushes

local function plane_intersect( p1, p2, p3 )
    local A1, B1, C1, D1 = p1.A, p1.B, p1.C, p1.D
    local A2, B2, C2, D2 = p2.A, p2.B, p2.C, p2.D
    local A3, B3, C3, D3 = p3.A, p3.B, p3.C, p3.D


    local det = (A1)*( B2*C3 - C2*B3 )
              - (B1)*( A2*C3 - C2*A3 )
              + (C1)*( A2*B3 - B2*A3 )

    if math.abs(det) < 0.001 then return nil end -- No intersection, planes must be parallel

    local x = (D1)*( B2*C3 - C2*B3 )
            - (B1)*( D2*C3 - C2*D3 )
            + (C1)*( D2*B3 - B2*D3 )

    local y = (A1)*( D2*C3 - C2*D3 )
            - (D1)*( A2*C3 - C2*A3 )
            + (C1)*( A2*D3 - D2*A3 )

    local z = (A1)*( B2*D3 - D2*B3 )
            - (B1)*( A2*D3 - D2*A3 )
            + (D1)*( A2*B3 - B2*A3 )

    return Vector(x,y,z)/det
end

local function is_point_inside_planes( planes, point )
    for i=1, #planes do
        local plane = planes[i]
        local t = point.x*plane.A + point.y*plane.B + point.z*plane.C
        if t - plane.D > 0.01 then return false end
    end
    return true
end

local function vertices_from_planes( planes )
    local verts = {}

    for i=1, #planes do
        local N1 = planes[i];

        for j=i+1, #planes do
            local N2 = planes[j]

            for k=j+1, #planes do
                local N3 = planes[k]

                local pVert = plane_intersect(N1, N2, N3)
                if pVert and is_point_inside_planes(planes,pVert) then
                    verts[#verts + 1] = pVert
                end
            end
        end
    end

    -- Filter out duplicate points
    local verts2 = {}
    for _, v1 in pairs(verts) do
        local exist = false
        for __, v2 in pairs(verts2) do
            if (v1-v2):LengthSqr() < 0.001 then
                exist = true
                break
            end
        end

        if not exist then
            verts2[#verts2 + 1] = v1
        end
    end

    return verts2
end

local function str2numbers( str )
    local ret = {}
    for k, v in pairs( string.Explode( " ", str ) ) do
        ret[k] = tonumber(v)
    end
    return unpack( ret )
end

local function find_uv( point, textureVecs, texSizeX, texSizeY )
    local x,y,z = point.x, point.y, point.z
    local u = textureVecs[1].x * x + textureVecs[1].y * y + textureVecs[1].z * z + textureVecs[1].offset
    local v = textureVecs[2].x * x + textureVecs[2].y * y + textureVecs[2].z * z + textureVecs[2].offset
    return u/texSizeX, v/texSizeY
end

local enums = {
    CONTENTS_EMPTY,
    CONTENTS_SOLID,
    CONTENTS_WINDOW,
    CONTENTS_AUX,
    CONTENTS_GRATE,
    CONTENTS_SLIME,
    CONTENTS_WATER,
    CONTENTS_BLOCKLOS,
    CONTENTS_OPAQUE,
    CONTENTS_TESTFOGVOLUME,
    CONTENTS_TEAM4,
    CONTENTS_TEAM3,
    CONTENTS_TEAM1,
    CONTENTS_TEAM2,
    CONTENTS_IGNORE_NODRAW_OPAQUE,
    CONTENTS_MOVEABLE,
    CONTENTS_AREAPORTAL,
    CONTENTS_PLAYERCLIP,
    CONTENTS_MONSTERCLIP,
    CONTENTS_CURRENT_0,
    CONTENTS_CURRENT_180,
    CONTENTS_CURRENT_270,
    CONTENTS_CURRENT_90,
    CONTENTS_CURRENT_DOWN,
    CONTENTS_CURRENT_UP,
    CONTENTS_DEBRIS,
    CONTENTS_DETAIL,
    CONTENTS_HITBOX,
    CONTENTS_LADDER,
    CONTENTS_MONSTER,
    CONTENTS_ORIGIN,
    CONTENTS_TRANSLUCENT,
}

local function GetBrushPhysics( self, single_mesh )
    self:LoadLumps( LUMP_BRUSHES, LUMP_BRUSHSIDES, LUMP_PLANES, LUMP_TEXINFO )

    local brushes = {}
    local brush_verts = {}

    for brush_id = 0, #self.lumps[LUMP_BRUSHES]["data"]-1 do
        local brush = self.lumps[LUMP_BRUSHES]["data"][brush_id]
        local brush_firstside = brush.firstside
        local brush_numsides = brush.numsides
        local brush_contents = brush.contents

        if bit.band( brush_contents, CONTENTS_PLAYERCLIP ) == 1 then continue end
        if bit.band( brush_contents, CONTENTS_AREAPORTAL ) == 1 then continue end
        if bit.band( brush_contents, CONTENTS_SOLID ) == 0 then continue end --TODO: store non-solidity

        local base_color = Vector(1,0,1)
        if not single_mesh then
            brush_verts = {}
        end

        brush.p_bsides = {}
        local planes = {}
        for i = 0, brush_numsides - 1 do
            local brushside_id = (brush_firstside + i)
            local brushside = self.lumps[LUMP_BRUSHSIDES]["data"][brushside_id]

            if brushside.bevel ~= 0 then continue end -- bevel != 0 means its used for physics collision, not interested
            local plane = self.lumps[LUMP_PLANES]["data"][brushside.planenum]
            brush.p_bsides[#brush.p_bsides + 1] = {
                brushside = brushside,
                plane = plane
            }
            planes[#planes + 1] = plane
        end

        brush.p_points = vertices_from_planes(planes)
        brush.p_render_data = {}
        for _, bside in pairs(brush.p_bsides) do
            local plane = bside.plane
            if not plane then continue end
            local render_data = {
                texinfo = bside.brushside.texinfo,
                plane = plane,
                points = {},
            }
            for __, point in pairs(brush.p_points) do
                local t = point.x*plane.A + point.y*plane.B + point.z*plane.C
                if math.abs(t-plane.D) > 0.01  then continue end -- Not on a plane

                render_data.points[#render_data.points + 1] = point
            end

            -- sort them in clockwise order
            local norm = Vector(plane.A, plane.B, plane.C)
            local c = render_data.points[1]
            table.sort(render_data.points, function(a, b)
                return norm:Dot((c-a):Cross(b-c)) > 0.001
            end)

            render_data.norm = norm

            local points = render_data.points
            local norm = render_data.norm
            local dot = math.abs( norm:Dot(Vector(-1,100,100):GetNormalized()) )
            local color = Color(100+55*dot,100+55*dot,100+55*dot) -- Color( 40357164 / 255 )
            color.r = color.r * base_color.x
            color.g = color.g * base_color.y
            color.b = color.b * base_color.z
            color.a = 255

            local texinfo = self.lumps[LUMP_TEXINFO]["data"][render_data.texinfo]

            local ref = Vector(0,0,-1)
            if math.abs( norm:Dot( Vector(0,0,1) ) ) == 1 then
                ref = Vector(0,1,0)
            end

            local tv1 = norm:Cross( ref ):Cross( norm ):GetNormalized()
            local tv2 = norm:Cross( tv1 )

            local textureVecs = {{x=tv2.x,y=tv2.y,z=tv2.z,offset=0},
                                {x=tv1.x,y=tv1.y,z=tv1.z,offset=0}}-- texinfo.textureVecs
            local u, v
            for j = 1, #points - 2 do
                u1, v1 = find_uv(points[1], textureVecs, 32, 32)
                u2, v2 = find_uv(points[j+1], textureVecs, 32, 32)
                u3, v3 = find_uv(points[j+2], textureVecs, 32, 32)
                --[[brush_verts[#brush_verts + 1] = { pos = points[1]+norm*0  , u = u1, v = v1, color = color }
                brush_verts[#brush_verts + 1] = { pos = points[j+1]+norm*0, u = u2, v = v2, color = color }
                brush_verts[#brush_verts + 1] = { pos = points[j+2]+norm*0, u = u3, v = v3, color = color }--]]
                brush_verts[#brush_verts + 1] = points[1]+norm*0
                brush_verts[#brush_verts + 1] = points[j+1]+norm*0
                brush_verts[#brush_verts + 1] = points[j+2]+norm*0
            end
        end

        --[[if not single_mesh then
            local obj = Mesh()
            obj:BuildFromTriangles( brush_verts )

            brush.p_mesh = obj
            brushes[#brushes+1] = obj
        end--]]
        if not single_mesh then
            brushes[#brushes+1] = brush_verts
        end
    end

    --[[if single_mesh then
        local obj = Mesh()
        obj:BuildFromTriangles( brush_verts )

        return obj
    end--]]

    if single_mesh then
        return brush_verts
    end

    return brushes
end

-- create the ent

easylua.StartEntity("cyn_mim_test")
DEFINE_BASECLASS( "base_anim" )
ENT.RenderGroup = RENDERGROUP_OPAQUE

function ENT:SetupDataTables()
	self:NetworkVar("Int", 0, "BrushIndex")
end

function ENT:UpdateTransmitState()
    return TRANSMIT_ALWAYS
end

function ENT:Initialize()
    --self:Setup(mapphys,mapmesh)
end

ENT.DisableDuplicator = true

function ENT:Setup(phys)
    self.phys = phys
    self:AddEFlags( EFL_FORCE_CHECK_TRANSMIT )
    self:SetModel("models/hunter/plates/plate.mdl")
	self:PhysicsInit(SOLID_VPHYSICS)
	self:PhysicsInitConvex(self.phys, false)
	self:EnableCustomCollisions()
	self:MakePhysicsObjectAShadow()

    if SERVER then
		self:SetMoveType(MOVETYPE_NOCLIP)
		self:SetMoveType(MOVETYPE_PUSH)
		self:SetCollisionGroup(COLLISION_GROUP_NONE)
		self:SetUseType(SIMPLE_USE)
	end

    if CLIENT then
        self.matrix = Matrix()
        self.time = RealTime()
    end
end

if CLIENT then
local mat = Material("models/wireframe")
function ENT:Draw()
    --self:DrawModel()
    --[[if not self.matrix then return end
    if self.mesh then
        self.matrix:SetAngles(self:GetAngles())
        self.matrix:SetTranslation(self:GetPos())
        cam.PushModelMatrix(self.matrix)
        render.SuppressEngineLighting( true )
        render.SetMaterial(mat)
        self.mesh:Draw()
		render.SuppressEngineLighting( false )
	    cam.PopModelMatrix()
    end--]]
end
end
easylua.EndEntity()

-- drawing

if CLIENT then
    local a = CreateMaterial( mat_name, "UnlitGeneric", {["$vertexalpha"] = 1} )

    hook.Add("PostDrawOpaqueRenderables","cyn_mim_test",function()
        for _,brush in pairs(ents.FindByClass("cyn_mim_test")) do

        end
    end)
end

-- setup the brush spawning

local mapphys = GetBrushPhysics(map,false)
local newmapphys = {}
local centers = {}
local total = table.Count(mapphys)

local function vectorCenter(vMap)
    local averages = Vector(0,0,0)

    for _,vec in ipairs(vMap) do
        averages.x = averages.x + vec.x
        averages.y = averages.y + vec.y
        averages.z = averages.z + vec.z
    end

    return Vector(averages.x / #vMap, averages.y / #vMap, averages.z / #vMap)
end

local function TableCopy(t, lookup_table)
	if (t == nil) then return nil end

	local copy = {}
	setmetatable(copy, getmetatable(t))
	for k,v in pairs(t) do
		if type(v) == "table" then
			lookup_table = lookup_table or {}
			lookup_table[t] = copy
			if lookup_table[v] then
				copy[k] = lookup_table[v] -- we already copied this table. reuse the copy.
			else
				copy[k] = TableCopy(v,lookup_table) -- not yet copied. copy it.
			end
		elseif type(v) == "Vector" then
			copy[k] = Vector(v.x, v.y, v.z)
		else
			copy[k] = v
		end
	end
	return copy
end

for index,points in ipairs(mapphys) do
    newmapphys[index] = newmapphys[index] or {}
    local center = vectorCenter(points)
    centers[index] = center
    for k,v in ipairs(points) do
        newmapphys[index][k] = v-center
    end
end

--PrintTable(mapphys[1])
--PrintTable(newmapphys[1])

--do return end

Msg("[MapInMap] ") MsgC(Color(255,255,255),"Processing "..total.." brushes...\n")

if SERVER then
    for i=1,total do
        --if i > 10 then continue end --testing
        timer.Simple(i/25,function()
            local m = ents.Create("cyn_mim_test")
            m:SetBrushIndex(i)
            m:Setup(newmapphys[i])
            m:SetPos(centers[i])
            m:Spawn()

            undo.Create("cyn_mim_test")
                undo.SetPlayer(me)
                undo.AddEntity(m)
            undo.Finish()

            me:AddCleanup("props", m)
        end)
    end
end

if CLIENT then
    local function GetBrushMeshes( self, single_mesh )
        self:LoadLumps( LUMP_BRUSHES, LUMP_BRUSHSIDES, LUMP_PLANES, LUMP_TEXINFO )

        local brushes = {}
        local brush_verts = {}

        for brush_id = 0, #self.lumps[LUMP_BRUSHES]["data"]-1 do
            local brush = self.lumps[LUMP_BRUSHES]["data"][brush_id]
            local brush_firstside = brush.firstside
            local brush_numsides = brush.numsides
            local brush_contents = brush.contents

            if bit.band( brush_contents, CONTENTS_PLAYERCLIP ) == 1 then continue end
            if bit.band( brush_contents, CONTENTS_AREAPORTAL ) == 1 then continue end
            if bit.band( brush_contents, CONTENTS_SOLID ) == 0 then continue end --TODO: store non-solidity

            local base_color = Vector(1,0,1)
            if not single_mesh then
                brush_verts = {}
            end

            brush.p_bsides = {}
            local planes = {}
            for i = 0, brush_numsides - 1 do
                local brushside_id = (brush_firstside + i)
                local brushside = self.lumps[LUMP_BRUSHSIDES]["data"][brushside_id]

                if brushside.bevel ~= 0 then continue end -- bevel != 0 means its used for physics collision, not interested
                local plane = self.lumps[LUMP_PLANES]["data"][brushside.planenum]
                brush.p_bsides[#brush.p_bsides + 1] = {
                    brushside = brushside,
                    plane = plane
                }
                planes[#planes + 1] = plane
            end

            brush.p_points = vertices_from_planes(planes)
            brush.p_render_data = {}
            for _, bside in pairs(brush.p_bsides) do
                local plane = bside.plane
                if not plane then continue end
                local render_data = {
                    texinfo = bside.brushside.texinfo,
                    plane = plane,
                    points = {},
                }
                for __, point in pairs(brush.p_points) do
                    local t = point.x*plane.A + point.y*plane.B + point.z*plane.C
                    if math.abs(t-plane.D) > 0.01  then continue end -- Not on a plane

                    render_data.points[#render_data.points + 1] = point
                end

                -- sort them in clockwise order
                local norm = Vector(plane.A, plane.B, plane.C)
                local c = render_data.points[1]
                table.sort(render_data.points, function(a, b)
                    return norm:Dot((c-a):Cross(b-c)) > 0.001
                end)

                render_data.norm = norm

                local points = render_data.points
                local norm = render_data.norm
                local dot = math.abs( norm:Dot(Vector(-1,100,100):GetNormalized()) )
                local color = Color(100+55*dot,100+55*dot,100+55*dot) -- Color( 40357164 / 255 )
                color.r = color.r * base_color.x
                color.g = color.g * base_color.y
                color.b = color.b * base_color.z
                color.a = 255

                local texinfo = self.lumps[LUMP_TEXINFO]["data"][render_data.texinfo]

                local ref = Vector(0,0,-1)
                if math.abs( norm:Dot( Vector(0,0,1) ) ) == 1 then
                    ref = Vector(0,1,0)
                end

                local tv1 = norm:Cross( ref ):Cross( norm ):GetNormalized()
                local tv2 = norm:Cross( tv1 )

                local textureVecs = {{x=tv2.x,y=tv2.y,z=tv2.z,offset=0},
                                    {x=tv1.x,y=tv1.y,z=tv1.z,offset=0}}-- texinfo.textureVecs
                local u, v
                for j = 1, #points - 2 do
                    u1, v1 = find_uv(points[1], textureVecs, 32, 32)
                    u2, v2 = find_uv(points[j+1], textureVecs, 32, 32)
                    u3, v3 = find_uv(points[j+2], textureVecs, 32, 32)
                    brush_verts[#brush_verts + 1] = { pos = points[1]+norm*0  , u = u1, v = v1, color = color }
                    brush_verts[#brush_verts + 1] = { pos = points[j+1]+norm*0, u = u2, v = v2, color = color }
                    brush_verts[#brush_verts + 1] = { pos = points[j+2]+norm*0, u = u3, v = v3, color = color }
                end
            end

            if not single_mesh then
                --[[local obj = Mesh()
                obj:BuildFromTriangles( brush_verts )

                brush.p_mesh = obj
                brushes[#brushes+1] = obj--]]
                brushes[#brushes+1] = brush_verts
            end
        end

        if single_mesh then
            --[[local obj = Mesh()
            obj:BuildFromTriangles( brush_verts )

            return obj--]]
            return brush_verts
        end

        return brushes
    end

    local meshes = {}
    local mapmesh = GetBrushMeshes(map,false)
    local newmapmesh = {}

    for index,points in ipairs(mapmesh) do
        newmapmesh[index] = newmapmesh[index] or {}
        local center = centers[index]
        for k,v in ipairs(points) do
            newmapmesh[index][k] = TableCopy(mapmesh[index][k])
            newmapmesh[index][k].pos = newmapmesh[index][k].pos-center
        end
    end

    for index,points in ipairs(newmapmesh) do
        local obj = Mesh()
        obj:BuildFromTriangles(points)
        meshes[index] = obj
    end

    hook.Add("NetworkEntityCreated","cyn_mim_test",function(ent)
        if not IsValid(ent) then return end
        if ent:GetClass() ~= "cyn_mim_test" then return end
        timer.Simple(0,function()
            local index = ent:GetBrushIndex()

            Msg("[MapInMap:CLIENT] ") MsgC(Color(255,255,255),"csinit brush: "..index.."/"..total.." | "..tostring(ent).."\n")
            ent.mesh = meshes[index]
            ent:Setup(newmapphys[index])
        end)
    end)
end