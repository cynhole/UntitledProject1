-- This prototype is based off reading a VMF file and trying to parse it
-- This didn't get very far but was the basis of me wanting to do a readable format apposed to BSPs

local brushes = {}

local function AverageVector(vMap)
    local averages = Vector(0,0,0)

    for _,vec in ipairs(vMap) do
        averages.x = averages.x + vec.x
        averages.y = averages.y + vec.y
        averages.z = averages.z + vec.z
    end

    return Vector(averages.x / #vMap, averages.y / #vMap, averages.z / #vMap)
end

local function FormatBrush(tbl)
    local id = #brushes + 1
    for _, kv in pairs(tbl) do
        brushes[id] = brushes[id] or {}
        local id2 = #brushes[id] + 1
        if kv.Key == "side" then
            for s_, skv in pairs(kv.Value) do
                brushes[id][id2] = brushes[id][id2] or {}
                if skv.Key == "plane" then
                    local temp = {}
                    for v in skv.Value:gmatch("%((.-)%)") do
                        local x, y, z = unpack(string.Explode(" ", v))
                        x = math.floor(x + 0.5)
                        y = math.floor(y + 0.5)
                        z = math.floor(z + 0.5)
                        table.insert(temp, Vector(x, y, z))
                    end

                    brushes[id][id2][skv.Key] = AverageVector(temp)
                else
                    brushes[id][id2][skv.Key] = skv.Value
                end
            end
        end
    end
end

local function ParseVMF(data)
    local vmf = util.KeyValuesToTablePreserveOrder(("map{%s}"):format(data))
    for _, kv in pairs(vmf) do
        if kv.Key == "world" then
            for w_, wkv in pairs(kv.Value) do
                if wkv.Key == "solid" then
                    FormatBrush(wkv.Value)
                end
            end
        end
    end
end

ParseVMF(file.Read("gm_construct-vmf.dat","DATA"))

_G.vmfbrushes = brushes

easylua.StartEntity("cyn_mim_test")
DEFINE_BASECLASS( "base_anim" )
ENT.RenderGroup = RENDERGROUP_OPAQUE

function ENT:UpdateTransmitState()
    return TRANSMIT_ALWAYS
end

function ENT:Initialize()
    --self:Setup(mapphys,mapmesh)
end

ENT.DisableDuplicator = true

function ENT:Setup(phys)
    self.phys = phys
    self:AddEFlags( EFL_FORCE_CHECK_TRANSMIT )
    self:SetModel("models/hunter/plates/plate.mdl")
    self:PhysicsInit(SOLID_VPHYSICS)
    self:PhysicsInitMultiConvex(self.phys, false)
    self:EnableCustomCollisions()
    self:MakePhysicsObjectAShadow()

    if SERVER then
        self:SetMoveType(MOVETYPE_NOCLIP)
        self:SetMoveType(MOVETYPE_PUSH)
        self:SetCollisionGroup(COLLISION_GROUP_NONE)
        self:SetUseType(SIMPLE_USE)
    end

    if CLIENT then
        self.matrix = Matrix()
        self.time = RealTime()
    end
end

if CLIENT then
    local mat = Material("models/wireframe")
    function ENT:Draw()
        self:DrawModel()
        --[[if not self.matrix then return end
        if self.phys then
            self.matrix:SetAngles(self:GetAngles())
            self.matrix:SetTranslation(self:GetPos())
            cam.PushModelMatrix(self.matrix)
            render.SuppressEngineLighting( true )
            render.SetMaterial(mat)
            self.phys:Draw()
            render.SuppressEngineLighting( false )
            cam.PopModelMatrix()
        end--]]
    end
end
easylua.EndEntity()

local mmesh = {}

for _, brush in pairs(brushes) do
    local b = {}
    for __, side in pairs(brush) do
        table.insert(b, side.plane)
    end
    table.insert(mmesh, b)
end

if SERVER then
    local m = ents.Create("cyn_mim_test")
    m:Setup(mmesh)
    m:SetPos(Vector(0,0,0))
    m:Spawn()

    undo.Create("cyn_mim_test")
        undo.SetPlayer(me)
        undo.AddEntity(m)
    undo.Finish()

    me:AddCleanup("props", m)
end

if CLIENT then
    hook.Add("HUDPaint", "cyn_mim_test", function()
        surface.SetDrawColor(255, 255, 255, 255)
        for _, b in pairs(mmesh) do
            local last
            for _, v in pairs(b) do
                if last then
                    local pos1 = last:ToScreen()
                    local pos2 = v:ToScreen()
                    if not pos1.visible or not pos2.visible then continue end
                    surface.DrawLine(pos1.x, pos1.y, pos2.x, pos2.y)
                    last = v
                else
                    last = v
                end
            end
        end
    end)
end