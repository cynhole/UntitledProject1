-- third prototype, furthest progress
-- ISSUES REMAINING:
-- * speed (this freezes both sv and cl for 5 seconds)
-- * displacements 
-- * lighting
-- * garbage collection of meshes, this leads to crashes after like 10 maps or so

-- RUN proto3-ent.lua FIRST

local luabsp = include("luabsp.lua")

local coroutine_yield = coroutine.yield
local pairs = pairs
local ipairs = ipairs
local Mesh = Mesh

if SERVER then
    util.AddNetworkString("cyn_mim_test")
end

if _G.mim_curmap and not IsValid(_G.mim_curmap.ent) then
    if CLIENT and _G.mim_curmap.brushes and _G.mim_curmap.brushes.meshes then
        --easylua.PrintOnServer("gc called")
        for _, m in pairs(_G.mim_curmap.brushes.meshes) do
            if IsValid(m) then
                m:Destroy()
            end
        end
        collectgarbage()
        collectgarbage()
    end
    _G.mim_curmap = {}
end

_G.mim_curmap = _G.mim_curmap or {}

_G.bsp_cache = _G.bsp_cache or {}
local MAP_NAME = "de_dust2"
local map = _G.bsp_cache[MAP_NAME] or luabsp.LoadMap(MAP_NAME)

if not _G.bsp_cache[MAP_NAME] then
    map:LoadLumps(LUMP_FACES, LUMP_TEXINFO, LUMP_SURFEDGES, LUMP_TEXDATA_STRING_TABLE, LUMP_TEXDATA_STRING_DATA, LUMP_TEXDATA, LUMP_EDGES, LUMP_VERTEXES, LUMP_DISPINFO, LUMP_DISP_VERTS, LUMP_BRUSHES, LUMP_BRUSHSIDES, LUMP_PLANES)
end

local function buildUV(edge, texinfo)
    local s = texinfo.textureVecs[1]
    local t = texinfo.textureVecs[2]

    local u = (s.offset + s.x * edge.x + s.y * edge.y + s.z * edge.z) / texinfo.texdata.width
    local v = (t.offset + t.x * edge.x + t.y * edge.y + t.z * edge.z) / texinfo.texdata.height

    return u, v
end

local function buildTri(buffer, vertex, u, v, norm)
    norm = norm or Vector(0, 0, 1)

    buffer[#buffer + 1] = {
        pos = vertex,
        u = u,
        v = v,
        normal = norm,
        userdata = {norm.x, norm.y, norm.z, -1}
    }

    local tris = {}

    if #buffer == 3 then
        for i = 1, 3 do
            tris[#tris + 1] = buffer[i]
        end

        table.remove(buffer, 2)
    end

    return tris
end

--[[local function To2D(source, orgpos, organg)
    local localpos = WorldToLocal(source, angle_zero, orgpos, organg)
    return Vector(localpos.y, localpos.z, 0)
end

local function To3D(source, orgpos, organg)
    local localpos = Vector(0, source.x, source.y)
    return LocalToWorld(localpos, angle_zero, orgpos, organg)
end

local function GetRotatedAABB(v2d, angle)
    local mins = Vector(math.huge, math.huge)
    local maxs = -mins
    for k, v in ipairs(v2d) do
        v = Vector(v)
        v:Rotate(angle)
        mins.x = math.min(mins.x, v.x)
        mins.y = math.min(mins.y, v.y)
        maxs.x = math.max(maxs.x, v.x)
        maxs.y = math.max(maxs.y, v.y)
    end

    return mins, maxs
end

local faceIndex = 0
local function makeSurface(mins, maxs, normal, angle, origin, v2d, v3d)
    if #v3d < 3 or faceIndex > (600000 or 1247232) then return end
    --[[local hitair = false
    for _, v in ipairs(v3d) do
        if bit.band(util.PointContents(v + normal * .01), CONTENTS_WATER) == 0 then
            hitair = true
            break
        end
    end
    if not hitair then return end--]]
    --[[faceIndex = faceIndex + 1

    local area, bound, minangle, minmins = math.huge, nil, nil, nil
    for i, v in ipairs(v2d) do --Get minimum AABB with O(n^2)
        local seg = v2d[i % #v2d + 1] - v
        local ang = Angle(0, 90 - math.deg(math.atan2(seg.y, seg.x)))
        local _mins, _maxs = GetRotatedAABB(v2d, ang)
        local tmpbound = _maxs - _mins
        if area > tmpbound.x * tmpbound.y then
            if tmpbound.x < tmpbound.y then
                ang.yaw = ang.yaw - 90
                minmins, _maxs = GetRotatedAABB(v2d, ang)
            else
                minmins = _mins
            end
            minangle = ang
            bound = _maxs - minmins
            area = bound.x * bound.y
        end
    end

    for i, v in ipairs(v2d) do
        v:Rotate(minangle)
        v:Sub(minmins)
    end

    minmins:Rotate(-minangle)
    origin = To3D(minmins, origin, angle)
    angle:RotateAroundAxis(normal, -minangle.yaw)

    local surf = {
        Angles = {},
        Areas = {},
        Bounds = {},
        Moved = {},
        Normals = {},
        Origins = {},
        Vertices = {},
        u = {},
        v = {},
    }
    table.insert(surf.Angles, angle)
    table.insert(surf.Areas, area)
    table.insert(surf.Bounds, bound)
    table.insert(surf.Normals, normal)
    table.insert(surf.Origins, origin)
    table.insert(surf.Vertices, v3d)

    local sortedsurfs, movesurfs = {}, {}
    local NumMeshTriangles, nummeshes, dv, divuv, half = 0, 1, 0, 1
    local u, v, nv, bu, bv, bk = 0, 0, 0

    for k in SortedPairsByValue(surf.Areas, true) do
        table.insert(sortedsurfs, k)
        NumMeshTriangles = NumMeshTriangles + #surf.Vertices[k] - 2

        --Using next-fit approach
        bu, bv = surf.Bounds[k].x, surf.Bounds[k].y
        nv = math.max(nv, bv)
        if u + bu > 1 then --Creating a new shelf
            if v + nv > 1 then table.insert(movesurfs, {id = bk, v = v}) end
            u, v, nv = 0, v + nv, bv
        end

        if u == 0 then bk = #sortedsurfs end --The first element of the current shelf
        for i, vertex in ipairs(surf.Vertices[k]) do --Get UV coordinates
            local meshvert = vertex + surf.Normals[k]
            local UV = To2D(meshvert, surf.Origins[k], surf.Angles[k])
            surf.Vertices[k][i] = {pos = meshvert, u = UV.x + u, v = UV.y + v, normal = surf.Normals[k]}
        end

        surf.u[k], surf.v[k] = u, v
        u = u + bu --Advance U-coordinate
    end

    return surf.Vertices[1]
end

local function buildDispTri(verts)
    local normal = (verts[1] - verts[2]):Cross(verts[3] - verts[2]):GetNormalized()
    local angle = normal:Angle()
    local origin = (verts[1] + verts[2] + verts[3]) / 3
    local mins = Vector(math.huge, math.huge, math.huge)
    local maxs = -mins

    local v2d = {}

    for i, v in ipairs(verts) do
        maxs.x = math.max(maxs.x, v.x)
        maxs.y = math.max(maxs.y, v.y)
        maxs.z = math.max(maxs.z, v.z)
        mins.x = math.min(mins.x, v.x)
        mins.y = math.min(mins.y, v.y)
        mins.z = math.min(mins.z, v.z)

        v2d[i] = To2D(v, origin, angle)
    end

    return makeSurface(mins, maxs, normal, angle, origin, v2d, verts)
end

local function VectorMA(start, scale, direction, dest)
    dest.x = start.x + direction.x * scale
    dest.y = start.y + direction.y * scale
    dest.z = start.z + direction.z * scale

    return dest
end

local function DoesEdgeExist(indexRow, indexCol, direction, postSpacing)
    if direction == 1 then -- left edge
        return (indexRow - 1) < 0 and false or true
    elseif direction == 2 then -- top edge
        return (indexCol + 1) > (postSpacing - 1) and false or true
    elseif direction == 3 then -- right edge
        return (indexRow + 1) > (postSpacing - 1) and false or true
    elseif direction == 4 then -- bottom edge
        return (indexCol - 1) < 0 and false or true
    else
        return false
    end
end

local function CalcNormalFromEdges(pVerts, indexRow, indexCol, bIsEdge, power)
    local normal = Vector()

    local postSpacing = (2 ^ power) + 1

    local accumNormal = Vector()
    local normalCount = 0

    local tmpVect = {}
    local tmpNormal

    if bIsEdge[2] and bIsEdge[3] then
        tmpVect[1] = pVerts[(indexCol + 1) * postSpacing + indexRow].vert - pVerts[indexCol * postSpacing + indexRow].vert
        tmpVect[2] = pVerts[indexCol * postSpacing + (indexRow + 1)].vert - pVerts[indexCol * postSpacing + indexRow].vert
        tmpNormal = tmpVect[2]:Cross(tmpVect[1])
        tmpNormal:Normalize()
        accumNormal = accumNormal + tmpNormal
        normalCount = normalCount + 1

        tmpVect[1] = pVerts[(indexCol + 1) * postSpacing + indexRow].vert - pVerts[indexCol * postSpacing + (indexRow + 1)].vert
        print("quad1-2-2", indexRow, indexCol, postSpacing, (indexCol + 1) * postSpacing + (indexRow + 1), indexCol * postSpacing + (indexRow + 1), pVerts[(indexCol + 1) * postSpacing + (indexRow + 1)], pVerts[indexCol * postSpacing + (indexRow + 1)])
        tmpVect[2] = pVerts[(indexCol + 1) * postSpacing + (indexRow + 1)].vert - pVerts[indexCol * postSpacing + (indexRow + 1)].vert
        tmpNormal = tmpVect[2]:Cross(tmpVect[1])
        tmpNormal:Normalize()
        accumNormal = accumNormal + tmpNormal
        normalCount = normalCount + 1
    end

    if bIsEdge[1] and bIsEdge[2] then
        --print("quad2-1-1", indexRow, indexCol, postSpacing, (indexCol + 1) * postSpacing + (indexRow - 1), pVerts[(indexCol + 1) * postSpacing + (indexRow - 1)], indexCol * postSpacing + (indexRow - 1), pVerts[indexCol * postSpacing + (indexRow - 1)])
        tmpVect[1] = pVerts[(indexCol + 1) * postSpacing + (indexRow - 1)].vert - pVerts[(indexCol * postSpacing + (indexRow - 1)) + 1].vert
        tmpVect[2] = pVerts[indexCol * postSpacing + indexRow].vert - pVerts[(indexCol * postSpacing + (indexRow - 1)) + 1].vert
        tmpNormal = tmpVect[2]:Cross(tmpVect[1])
        tmpNormal:Normalize()
        accumNormal = accumNormal + tmpNormal
        normalCount = normalCount + 1

        tmpVect[1] = pVerts[(indexCol + 1) * postSpacing + (indexRow - 1)].vert - pVerts[indexCol * postSpacing + indexRow].vert
        tmpVect[2] = pVerts[(indexCol + 1) * postSpacing + indexRow].vert - pVerts[indexCol * postSpacing + indexRow].vert
        tmpNormal = tmpVect[2]:Cross(tmpVect[1])
        tmpNormal:Normalize()
        accumNormal = accumNormal + tmpNormal
        normalCount = normalCount + 1
    end

    if bIsEdge[1] and bIsEdge[4] then
        tmpVect[1] = pVerts[indexCol * postSpacing + (indexRow - 1)].vert - pVerts[(indexCol - 1) * postSpacing + (indexRow - 1)].vert
        tmpVect[2] = pVerts[(indexCol - 1) * postSpacing + indexRow].vert - pVerts[(indexCol - 1) * postSpacing + (indexRow - 1)].vert
        tmpNormal = tmpVect[2]:Cross(tmpVect[1])
        tmpNormal:Normalize()
        accumNormal = accumNormal + tmpNormal
        normalCount = normalCount + 1

        tmpVect[1] = pVerts[indexCol * postSpacing + (indexRow - 1)].vert - pVerts[(indexCol - 1) * postSpacing + indexRow].vert
        tmpVect[2] = pVerts[indexCol * postSpacing + indexRow].vert - pVerts[(indexCol - 1) * postSpacing + indexRow].vert
        tmpNormal = tmpVect[2]:Cross(tmpVect[1])
        tmpNormal:Normalize()
        accumNormal = accumNormal + tmpNormal
        normalCount = normalCount + 1
    end

    if bIsEdge[3] and bIsEdge[4] then
        tmpVect[1] = pVerts[indexCol * postSpacing + indexRow].vert - pVerts[(indexCol - 1) * postSpacing + indexRow].vert
        tmpVect[2] = pVerts[(indexCol - 1) * postSpacing + (indexRow + 1)].vert - pVerts[(indexCol - 1) * postSpacing + indexRow].vert
        tmpNormal = tmpVect[2]:Cross(tmpVect[1])
        tmpNormal:Normalize()
        accumNormal = accumNormal + tmpNormal
        normalCount = normalCount + 1

        tmpVect[1] = pVerts[indexCol * postSpacing + indexRow].vert - pVerts[(indexCol - 1) * postSpacing + (indexRow + 1)].vert
        tmpVect[2] = pVerts[indexCol * postSpacing + (indexRow + 1)].vert - pVerts[(indexCol - 1) * postSpacing + (indexRow + 1)].vert
        tmpNormal = tmpVect[2]:Cross(tmpVect[1])
        tmpNormal:Normalize()
        accumNormal = accumNormal + tmpNormal
        normalCount = normalCount + 1
    end

    normal = accumNormal * (1 / normalCount)

    return normal
end--]]

local function getBrushes(self)
    local collision = {}
    local meshes = {}
    local materials = {}

    local faces = self.lumps[LUMP_FACES]["data"]
    local texinfo = self.lumps[LUMP_TEXINFO]["data"]
    local verts = self.lumps[LUMP_VERTEXES]["data"]
    local edges = self.lumps[LUMP_EDGES]["data"]
    local surfedges = self.lumps[LUMP_SURFEDGES]["data"]
    local planes = self.lumps[LUMP_PLANES]["data"]

    --local eCount = #edges
    if not self.edgesRemapped then
        for k, edge in ipairs(edges) do
            -- if not verts[edge[1]] then coroutine_yield(false, Format("Remapping edges from vertices (%d/%d) [SKIPPED, NO VERT FOR EDGE 1]", k, eCount)) continue end
            -- if not verts[edge[2]] then coroutine_yield(false, Format("Remapping edges from vertices (%d/%d) [SKIPPED, NO VERT FOR EDGE 2]", k, eCount)) continue end

            edge[1] = verts[edge[1]]
            edge[2] = verts[edge[2]]

            coroutine_yield(false, "Remapping edges from vertices")
            --coroutine_yield(false, Format("Remapping edges from vertices (%d/%d)", k, eCount))
        end

        self.edgesRemapped = true
    end

    --local seCount = #surfedges
    if not self.surfedgesRemapped then
        for k, surfedge in ipairs(surfedges) do
            -- if surfedge > 0 and not edges[surfedge] then coroutine_yield(false, Format("Remapping surfedges from edges (%d/%d) [SKIPPED, NO EDGE FOR SURFEDGE]", k, seCount)) continue end
            -- if surfedge < 0 and not edges[-surfedge] then coroutine_yield(false, Format("Remapping surfedges from edges (%d/%d) [SKIPPED, NO EDGE FOR -SURFEDGE]", k, seCount)) continue end

            surfedges[k] = surfedge > 0 and edges[surfedge] or { edges[-surfedge][2], edges[-surfedge][1] }

            coroutine_yield(false, "Remapping surfedges from edges")
            -- coroutine_yield(false, Format("Remapping surfedges from edges (%d/%d)", k, seCount))
        end

        self.surfedgesRemapped = true
    end

    local mats = {}
    --local fCount = #faces
    for i, face in ipairs(faces) do
        local data = texinfo[face.texinfo]
        if not data then --[[coroutine_yield(false, Format("Processing materials (%d/%d) [SKIPPED, NO DATA]", i, fCount))--]] continue end

        local texindex = self.lumps[LUMP_TEXDATA_STRING_TABLE]["data"][data.textdata]
        local texpath = self.lumps[LUMP_TEXDATA_STRING_DATA]["data"][texindex]

        if texpath:find("^maps/") then
            texpath = texpath:gsub("(_%-?%d%d?%d?%d?%d?_%-?%d%d?%d?%d?%d?_%-?%d%d?%d?%d?%d?)",""):gsub("maps/(.-)/","")
        end

        data.texdata = self.lumps[LUMP_TEXDATA]["data"][data.textdata]
        data.texdata.material = texpath

        if texpath:sub(1, 6):lower() == "tools/" and (not texpath:find("skybox") or not texpath:find("nodraw")) then --[[coroutine_yield(false, Format("Processing materials (%d/%d) [SKIPPED, TOOL MAT]", i, fCount))--]] continue end
        if texpath:find("water") then --[[coroutine_yield(false, Format("Processing materials (%d/%d) [SKIPPED, WATER]", i, fCount))--]] continue end

        mats[data] = mats[data] or {}

        table.insert(mats[data], face)

        coroutine_yield(false, "Processing materials")
        --coroutine_yield(false, Format("Processing materials (%d/%d)", i, fCount))
    end

    for data, material in pairs(mats) do
        local triangles = {}

        local info = texinfo[material[1].texinfo]

        local msh = {}

        if CLIENT then
            msh = Mesh()
            local _mat = Material(info.texdata.material)
            local mat = CreateMaterial("mim-" .. tostring(info), "VertexLitGeneric", {
                ["$basetexture"] = _mat:IsError() and "dev/dev_measuregeneric01b" or _mat:GetTexture("$basetexture") and _mat:GetTexture("$basetexture"):GetName() or "dev/dev_measuregeneric01b",
                ["$detailscale"] = 1,
                ["$reflectivity"] = util.StringToType(info.texdata.reflectivity, "Vector"),
                ["$model"] = 1,
            })

            table.insert(meshes, msh)
            table.insert(materials, mat)

            --coroutine_yield(false, "Creating meshes and materials")
        end

        --local count = #material
        for e, face in ipairs(material) do
            local buffer = {}

            local plane = planes[face.planenum]
            local norm = Vector(plane.A, plane.B, plane.C)

            if face.dispinfo ~= -1 then
                local disp = self.lumps[LUMP_DISPINFO]["data"][face.dispinfo]
                local dispverts = {}

                for i = 0, ((2 ^ disp.power + 1) ^ 2) - 1 do
                    dispverts[i] = self.lumps[LUMP_DISP_VERTS]["data"][disp.DispVertStart + i]
                end

                local points = {}

                for i = 0, face.numedges - 1 do
                    local surfedge = surfedges[face.firstedge + i]

                    local vertex1 = surfedge[1]
                    --local vertex2 = surfedge[2]

                    table.insert(points, vertex1)
                    --table.insert(points, vertex2)

                    --[[local u1, v1 = buildUV(vertex1, data)

                    table.Add(fverts, buildTri(buffer, vertex1, u1, v1, norm))

                    local u2, v2 = buildUV(vertex2, data)

                    table.Add(fverts, buildTri(buffer, vertex2, u2, v2, norm))--]]
                end

                --[[local deduped = {}
                for _, v in pairs(fverts) do
                    if deduped[v.pos] then continue end
                    deduped[v.pos] = v
                end

                local points = {}
                for _, v in pairs(deduped) do
                    points[#points + 1] = v.pos
                end--]]

                --[[local mins = Vector(math.huge, math.huge, math.huge)
                local maxs = -mins

                for _, vert in pairs(points) do
                    mins.x = math.min(mins.x, vert.x)
                    mins.y = math.min(mins.y, vert.y)
                    mins.z = math.min(mins.z, vert.z)
                    maxs.x = math.max(maxs.x, vert.x)
                    maxs.y = math.max(maxs.y, vert.y)
                    maxs.z = math.max(maxs.z, vert.z)
                end

                points = {
                    Vector(maxs.x, mins.y, maxs.z),
                    Vector(mins.x, mins.y, maxs.z),
                    Vector(mins.x, maxs.y, mins.z),
                    Vector(maxs.x, maxs.y, mins.z),
                }--]]

                --[[local indice, min, start = {}, math.huge, 0
                for k, v in ipairs(points) do
                    local dist = disp.start:DistToSqr(v)
                    if dist < min then start, min = k, dist end
                end

                for k = 1, 4 do indice[k] = (k + start - 2) % 4 + 1 end
                points[1], points[2], points[3], points[4] = points[indice[1] ], points[indice[2] ], points[indice[3] ], points[indice[4] ]--]]

                --[[points = {
                    points[3],
                    points[4],
                    points[1],
                    points[2],
                }--]]

                --print(norm)

                points = {
                    points[2],
                    points[1],
                    points[4],
                    points[3],
                }

                local startIndex
                for i, p in ipairs(points) do
                    if p == disp.start then
                        startIndex = i
                        break
                    end
                end

                if startIndex then
                    local out = {}
                    for i = 1, #points do
                        local point = (startIndex + (i - 1)) % (#points + 1)
                        if point == 0 then
                            point = point + 1
                            startIndex = startIndex + 1
                        end
                        out[#out + 1] = points[point]
                    end
                    points = out
                end

                points = {
                    points[2],
                    points[3],
                    points[4],
                    points[1],
                }

                --[[local tris = {}

                local u1, u2 = points[4] - points[1], points[3] - points[2]
                local v1, v2 = points[2] - points[1], points[3] - points[4]

                for k, v in pairs(dispverts) do
                    localx = k % disp.power
                    localy = math.floor(k / disp.power)
                    local div1 = v1 * y / (disp.power - 1)
                    local div2 = u1 + v2 * y / (disp.power - 1) - div1
                    v.origin = div1 + div2 * x / (disp.power - 1)
                    v.pos = disp.start + v.origin + v.vec * v.dist
                    --v.posgrid = v.pos - norm * norm:Dot(v.vec * v.dist)
                    --disp.Vertices[#disp.Vertices + 1] = v.pos
                    --disp.VerticesGrid[#disp.VerticesGrid + 1] = v.posgrid
                end

                for k, v in pairs(dispverts) do
                    local x = k % disp.power
                    local y = math.floor(k / disp.power)

                    local invert = Either(k % 2 == 1, 1, 0)
                    if x < disp.power - 1 and y < disp.power - 1 then
                        table.Add(tris, buildDispTri({dispverts[k + disp.power + invert].pos, dispverts[k + 1].pos, dispverts[k].pos}))
                        table.Add(tris, buildDispTri({dispverts[k + 1 - invert].pos, dispverts[k + disp.power].pos, dispverts[k + disp.power + 1].pos}))
                    end
                end--]]

                --PrintTable(tris)

                --[[local disptris = {}
                for _, v in pairs(tris) do
                    local _u, _v = buildUV(v, data)
                    table.Add(disptris, buildTri(buffer, v, _u, _v, norm))
                end
                PrintTable(disptris)--]]

                -- generate displacement surface
                local postSpacing = 2 ^ disp.power + 1
                local ooInt = 1 / (postSpacing - 1)

                local edgeInt = {}
                edgeInt[1] = (points[2] - points[1]) * ooInt
                edgeInt[2] = (points[3] - points[4]) * ooInt

                -- TODO: figure out m_Elevation

                for i = 0, postSpacing - 1 do
                    local endPts = {}
                    endPts[1] = (edgeInt[1] * i) + points[1]
                    endPts[2] = (edgeInt[2] * i) + points[4]

                    local seg, segInt
                    seg = endPts[1] - endPts[2]
                    segInt = seg * ooInt

                    for j = 0, postSpacing - 1 do
                        local index = i * postSpacing + j

                        local pVert = dispverts[index]
                        pVert.flatVert = endPts[2] + (segInt * j)
                        pVert.vert = pVert.flatVert

                        -- TODO: ^^ += elevation
                        -- += m_SubDivPos

                        pVert.vert = pVert.vert + (pVert.vec * pVert.dist)
                    end
                end

                local grid = {}
                for y = 0, postSpacing - 1 do
                    grid[y] = {}
                end

                for y = 0, postSpacing - 1 do
                    for x = 0, postSpacing - 1 do
                        local index = x * postSpacing + y
                        grid[y][x] = dispverts[index].vert
                    end
                end

                local tris = {}
                for y = 0, postSpacing - 1 do
                    for x = 0, postSpacing - 1 do
                        if x + 1 > (postSpacing - 1) or y + 1 > (postSpacing - 1) then continue end

                        local tri1 = {grid[y][x], grid[y][x + 1], grid[y + 1][x]}
                        local tri2 = {grid[y + 1][x + 1], grid[y + 1][x], grid[y][x + 1]}

                        for i, vert in pairs(tri1) do
                            local u, v = buildUV(vert, data)
                            tri1[i] = {pos = vert, u = u, v = v, normal = norm}
                        end
                        for i, vert in pairs(tri2) do
                            local u, v = buildUV(vert, data)
                            tri2[i] = {pos = vert, u = u, v = v, normal = norm}
                        end

                        --PrintTable(tri1, tri2)

                        table.Add(tris, tri1)
                        table.Add(tris, tri2)

                        --[[debugoverlay.Line(tri1[1].pos, tri1[2].pos, 15, Color(0, 0, 255), true)
                        debugoverlay.Line(tri1[2].pos, tri1[3].pos, 15, Color(0, 0, 255), true)
                        debugoverlay.Line(tri1[3].pos, tri1[1].pos, 15, Color(0, 0, 255), true)

                        debugoverlay.Line(tri2[1].pos, tri2[2].pos, 15, Color(0, 0, 255), true)
                        debugoverlay.Line(tri2[2].pos, tri2[3].pos, 15, Color(0, 0, 255), true)
                        debugoverlay.Line(tri2[3].pos, tri2[1].pos, 15, Color(0, 0, 255), true)--]]
                    end
                end

                --[[debugoverlay.Text(points[1], "1: " .. tostring(points[1]), 15)
                debugoverlay.Text(points[2], "2: " .. tostring(points[2]), 15)
                debugoverlay.Text(points[3], "3: " .. tostring(points[3]), 15)
                debugoverlay.Text(points[4], "4: " .. tostring(points[4]), 15)

                debugoverlay.Line(points[1], points[2], 15, Color(255, 0, 0), true)
                debugoverlay.Line(points[2], points[3], 15, Color(255, 0, 0), true)
                debugoverlay.Line(points[3], points[4], 15, Color(255, 0, 0), true)
                debugoverlay.Line(points[4], points[1], 15, Color(255, 0, 0), true)--]]

                --PrintTable(tris)

                --PrintTable(dispverts)

                --[[local disptris = {}
                local div = math.sqrt(#dispverts + 1)
                for _, v in pairs(dispverts) do
                    --print(_, v.flatVert, v.vert)
                    local pos = v.vert
                    local _u, _v = buildUV(pos, data)
                    table.insert(disptris, {pos = pos, u = _u, v = _v, norm = norm})
                end--]]

                --print(#disptris, #_disptris, #disptris % 3, #_disptris % 3)

                -- generate normals
                --[[for i = 0, postSpacing - 1 do
                    for j = 0, postSpacing - 1 do
                        local bIsEdge = {}
                        for k = 1, 3 do
                            bIsEdge[k] = DoesEdgeExist(j, i, k, postSpacing)
                        end

                        dispverts[i * postSpacing + j].normal = CalcNormalFromEdges(dispverts, j, i, bIsEdge, disp.power)
                    end
                end--]]

                --PrintTable("Four Points", _verts)
                --PrintTable("dispverts", dispverts)
                --PrintTable("disptris", disptris)

                --[[PrintTable("Four Points", _verts)

                local minX, minY, minZ, maxX, maxY, maxZ

                for _, v in pairs(_verts) do
                    minX = minX and math.min(v.x, minX) or v.x
                    minY = minY and math.min(v.y, minY) or v.y
                    minZ = minZ and math.min(v.z, minZ) or v.z
                    maxX = maxX and math.max(v.x, maxX) or v.x
                    maxY = maxY and math.max(v.y, maxY) or v.y
                    maxZ = maxZ and math.max(v.z, maxZ) or v.z
                end

                local O = Vector(minX, maxY, minZ)
                local X = Vector(minX, minY, minZ) - O
                local Y = Vector(maxX, maxY, maxZ) - O


                PrintTable("O, X, Y", O, X, Y)

                local M = {
                    {X.x, Y.x, 0},
                    {X.y, Y.y, 0},
                    {X.z, Y.z, 1},
                }

                PrintTable("M", M)

                local det = M[1][1] * (M[2][2] * M[3][3] - M[3][2] * M[2][3])
                det = det - M[1][2] * (M[2][1] * M[3][3] - M[2][3] * M[3][1])
                det = det + M[1][3] * (M[2][1] * M[3][2] - M[2][2] * M[3][1])

                local invdet = 1 / det

                local Minv = {
                    {
                        (M[2][2] * M[3][3] - M[3][2] * M[2][3]) * invdet,
                        (M[1][3] * M[3][2] - M[1][2] * M[3][3]) * invdet,
                        (M[1][2] * M[2][3] - M[1][3] * M[2][2]) * invdet,
                    },
                    {
                        (M[2][3] * M[3][1] - M[2][1] * M[3][3]) * invdet,
                        (M[1][1] * M[3][3] - M[1][3] * M[3][1]) * invdet,
                        (M[2][1] * M[1][3] - M[1][1] * M[2][3]) * invdet,
                    },
                    {
                        (M[2][1] * M[3][2] - M[3][1] * M[2][2]) * invdet,
                        (M[3][1] * M[1][2] - M[1][1] * M[3][2]) * invdet,
                        (M[1][1] * M[2][2] - M[2][1] * M[1][2]) * invdet,
                    }
                }

                PrintTable("Minv", Minv)

                local norms = {}
                for _, v in pairs(fverts) do
                    local vert = v.pos - O
                    norms[#norms + 1] = Vector((vert.x * Minv[1][1]) + (vert.y * Minv[1][2]) + (vert.z * Minv[1][3]), (vert.x * Minv[2][1]) + (vert.y * Minv[2][2]) + (vert.z * Minv[2][3]), (vert.x * Minv[3][1]) + (vert.y * Minv[3][2]) + (vert.z * Minv[3][3]))
                end

                PrintTable("Normalized", norms)

                local asdf = {}
                for _, v in pairs(dispverts) do
                    local vert = v.vec * v.dist
                    asdf[#asdf + 1] = Vector((vert.x * M[1][1]) + (vert.y * M[1][2]) + (vert.z * M[1][3]), (vert.x * M[2][1]) + (vert.y * M[2][2]) + (vert.z * M[2][3]), (vert.x * M[3][1]) + (vert.y * M[3][2]) + (vert.z * M[3][3])) + O
                end

                PrintTable(asdf)--]]

                --[[local disptris = {}
                for k, v in pairs(dispverts) do
                    for _, vert in pairs(_verts) do
                        local pos = vert.pos + (v.vec * v.dist)

                        local _u, _v = buildUV(pos, data)

                        table.insert(disptris, {pos = pos, u = _u, v = _v, normal = norm})
                    end
                end--]]

                table.Add(triangles, tris)

                --break
            else
                for i = 0, face.numedges - 1 do
                    local surfedge = surfedges[face.firstedge + i]
                    local vert = surfedge[1]

                    local u, v = buildUV(vert, data)
                    table.Add(triangles, buildTri(buffer, vert, u, v, norm))
                end
            end

            --[[local dispinfo = face.dispinfo
            if dispinfo ~= -1 then
                dispinfo = self.lumps[LUMP_DISPINFO]["data"][dispinfo]
                plane_to_disp[face.planenum] = dispinfo

                local vertex1 = util.StringToType(surfedge[1], "Vector")
                local vertex2 = util.StringToType(surfedge[2], "Vector")

                local start = dispinfo.start
                local dist =

                for j = 0, (2 ^ dispinfo.power + 1) ^ 2 do
                    local vert = self.lumps[LUMP_DISP_VERTS]["data"][dispinfo.DispVertStart + j]

                    local pos1 = vertex1 + (vert.vec * vert.dist)
                    local pos2 = vertex2 + (vert.vec * vert.dist)

                    local u1, v1 = buildUV(pos1, data)
                    local u2, v2 = buildUV(pos2, data)

                    table.Add(triangles, buildTri(buffer, pos1, u1, v1))
                    table.Add(triangles, buildTri(buffer, pos2, u2, v2))
                end
            else--]]

            coroutine_yield(false, "Building surfaces")
            --coroutine_yield(false, Format("Building surfaces | %s - %d/%d", info.texdata.material, e, count))
        end

        if CLIENT then
            msh:BuildFromTriangles(triangles)
            coroutine_yield(false, "Building meshes")
        end

        for _, tri in pairs(triangles) do
            table.insert(collision, {pos = tri.pos})
            coroutine_yield(false, "Building collision")
        end
    end

    return {meshes = meshes, materials = materials, collision = collision}
end

if not map.static_props then
    map:LoadStaticProps()
end

local threads = {}
local THRESHOLD = 0.002

hook.Add("Think", "mim_threader", function()
    for i, thread in ipairs(threads) do
        local t0 = SysTime()
        local succ, finished, msg
        while SysTime() - t0 < THRESHOLD do
            succ, finished, msg = coroutine.resume(thread)

            if not succ then break end
            if finished then break end
        end

        if not succ then
            table.remove(threads, i)
            error(finished)
        elseif finished then
            table.remove(threads, i)
            Msg("[MapInMap] ") print("Finished!")
        else
            Msg("[MapInMap] ") print(msg)
        end

        break
    end
end)

if SERVER then
    net.Receive("cyn_mim_test", function(len, ply)
        net.Start("cyn_mim_test")
        net.WriteEntity(_G.mim_curmap.ent)
        net.Send(ply)
    end)
end

if CLIENT then
    net.Receive("cyn_mim_test", function()
        local ent = net.ReadEntity()
        if not IsValid(ent) then return end
        if ent:GetClass() ~= "cyn_mim_test" then return end

        timer.Simple(0, function()
            timer.Simple(0, function()
                ent:Setup(_G.mim_curmap.brushes)
            end)
        end)
    end)
end

local function createMap(b)
    if SERVER then
        local m = ents.Create("cyn_mim_test")
        m:SetPos(Vector(0,0,0))
        m:Setup(b)
        m:Spawn()
        _G.mim_curmap.ent = m
        --timer.Simple(0, function()
        --    m:SetInstance(MAP_NAME)
        --end)

        undo.Create("cyn_mim_test")
        undo.SetPlayer(me)
        undo.AddEntity(m)

        m.static_props = {}

        for _, leaf in pairs(map.static_props) do
            for _, prop in pairs(leaf.entries) do
                if not prop.PropType then continue end
                local p = ents.Create(--[[util.IsValidProp(prop.PropType) and "prop_physics" or --]]"prop_dynamic")
                p:SetModel(prop.PropType)
                p:SetPos(m:GetPos() + prop.Origin)
                p:SetAngles(prop.Angles)
                p:SetSkin(prop.Skin)
                p:PhysicsInit(SOLID_VPHYSICS)
                p:PhysicsInitShadow(false, false)
                p:Spawn()
                p.PhysgunDisabled = true
                constraint.NoCollide(m, p, 0, 0)

                local phys = p:GetPhysicsObject()
                if IsValid(phys) then
                    phys:EnableMotion(false)
                end


                --timer.Simple(0, function()
                --    p:SetInstance(MAP_NAME)
                --end)

                m.static_props[#m.static_props + 1] = p

                undo.AddEntity(p)

                coroutine_yield(false, "Creating static props")
            end
        end
        undo.Finish()

        me:AddCleanup("props", m)
    end

    if CLIENT then
        net.Start("cyn_mim_test")
        net.SendToServer()
    end
end

local lumps = {LUMP_FACES, LUMP_TEXINFO, LUMP_SURFEDGES, LUMP_TEXDATA_STRING_TABLE, LUMP_TEXDATA_STRING_DATA, LUMP_TEXDATA, LUMP_EDGES, LUMP_VERTEXES, LUMP_DISPINFO, LUMP_DISP_VERTS, LUMP_BRUSHES, LUMP_BRUSHSIDES, LUMP_PLANES}

local function processMap(m)
    local co = coroutine.create(function()
        if not _G.bsp_cache[MAP_NAME] then
            for _, lump in ipairs(lumps) do
                map:LoadLumps(lump)
                coroutine_yield(false, "Loading lumps")
            end
        end

        local brushes = getBrushes(m)

        if brushes then
            _G.mim_curmap = {
                bsp = map,
                brushes = brushes,
            }

            createMap(brushes)

            if not _G.bsp_cache[MAP_NAME] then
                _G.bsp_cache[MAP_NAME] = map
            end
        end

        coroutine_yield(true)
    end)

    table.insert(threads, co)
end

processMap(map)

--[[local matrix = Matrix()
local ent = _720
hook.Add("PostDrawOpaqueRenderables", "mim_testing", function(depth, sky)
    matrix:SetAngles(ent:GetAngles())
    matrix:SetTranslation(ent:GetPos())

    cam.PushModelMatrix(matrix)
    --render.SetMaterial(mat)
    if brushes and istable(brushes) and brushes.meshes and istable(brushes.meshes) then
        for i, m in pairs(brushes.meshes) do
            if not IsValid(m) or m == NULL then
                table.remove(brushes.meshes, i)
            else
                render.SetMaterial(brushes.materials[i])
                m:Draw()
            end
        end
    end

    cam.PopModelMatrix()
end)--]]

--do return end
----

do
    if SERVER then
        util.AddNetworkString("cyn_mim_test")

        local m = ents.Create("cyn_mim_test")
        m:SetPos(Vector(0,0,0))
        m:Setup(brushes)
        m:Spawn()
        _G.mim_curmap.ent = m
        --timer.Simple(0, function()
        --    m:SetInstance(MAP_NAME)
        --end)

        net.Start("cyn_mim_test")
        net.WriteEntity(m)
        net.Broadcast()

        undo.Create("cyn_mim_test")
        undo.SetPlayer(me)
        undo.AddEntity(m)

        for _, leaf in pairs(map.static_props) do
            for _, prop in pairs(leaf.entries) do
                if not prop.PropType then continue end
                local p = ents.Create(util.IsValidProp(prop.PropType) and "prop_physics" or "prop_dynamic")
                p:SetModel(prop.PropType)
                p:SetPos(prop.Origin)
                p:SetAngles(prop.Angles)
                p:SetSkin(prop.Skin)
                p:Spawn()
                p.PhysgunEnabled = false

                local phys = p:GetPhysicsObject()
                if IsValid(phys) then
                    phys:EnableMotion(false)
                end

                --timer.Simple(0, function()
                --    p:SetInstance(MAP_NAME)
                --end)

                undo.AddEntity(p)
            end
        end
        undo.Finish()

        me:AddCleanup("props", m)
    end

    if CLIENT then
        net.Receive("cyn_mim_test", function()
            local ent = net.ReadEntity()
            if not IsValid(ent) then return end
            if ent:GetClass() ~= "cyn_mim_test" then return end

            timer.Simple(0, function()
                timer.Simple(0, function()
                    ent:Setup(brushes)
                end)
            end)
        end)
    end
end