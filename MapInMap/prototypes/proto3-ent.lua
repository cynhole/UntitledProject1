-- third prototype, furthest progress
-- RUN ME FIRST

easylua.StartEntity("cyn_mim_test")
DEFINE_BASECLASS( "base_anim" )
ENT.RenderGroup = RENDERGROUP_OPAQUE

function ENT:UpdateTransmitState()
    return TRANSMIT_ALWAYS
end

function ENT:Initialize()
end

ENT.DisableDuplicator = true
ENT.PhysgunEnabled = false

function ENT:Setup(b)
    self.brushes = b
    self:SetModel("models/hunter/plates/plate.mdl")

    local init = self:PhysicsFromMesh(b.collision)
    print(self, init)

    if SERVER then
        self:SetUseType(SIMPLE_USE)
        self:SetSolid(SOLID_VPHYSICS)
        self:SetMoveType(MOVETYPE_NONE)
        self:EnableCustomCollisions(true)

        local phys = self:GetPhysicsObject()
        if IsValid(phys) then
            phys:EnableMotion(false)
        end
    end

    if CLIENT then
        self:SetRenderBounds(Vector(-1e7, -1e7, -1e7), Vector(1e7, 1e7, 1e7))
    end
end

function ENT:OnRemove()
    -- GARBAGE DAY
    if CLIENT and self.brushes and istable(self.brushes) and self.brushes.meshes and istable(self.brushes.meshes) then
        easylua.PrintOnServer("calling on remove???")
        for _, m in pairs(self.brushes.meshes) do
            if IsValid(m) then
                m:Destroy()
            end
        end
        collectgarbage()
        collectgarbage()
    end
end

if CLIENT then
    function ENT:Think()
        self.shouldDraw = false
    end
    function ENT:Draw()
        self.shouldDraw = true -- lazy instances support and whatnot
    end

    --local mat = Material("models/wireframe") --Material("models/debug/debugwhite") --MapPatcher.Editor.GenerateToolMaterial( "cyn_mim", Color(128,128,128,192), "" )
    hook.Add("PostDrawOpaqueRenderables", "cyn_mim", function(depth, sky)
        for _, ent in pairs(ents.FindByClass("cyn_mim_test")) do
            if not ent.shouldDraw then continue end

            ent.__matrix = ent.__matrix or Matrix()
            ent.__matrix:SetAngles(ent:GetAngles())
            ent.__matrix:SetTranslation(ent:GetPos())

            cam.PushModelMatrix(ent.__matrix)
            --render.SetMaterial(mat)
            if ent.brushes and istable(ent.brushes) and ent.brushes.meshes and istable(ent.brushes.meshes) then
                for i, m in pairs(ent.brushes.meshes) do
                    if not IsValid(m) or m == NULL then
                        table.remove(ent.brushes.meshes, i)
                    else
                        render.SetMaterial(ent.brushes.materials[i])
                        m:Draw()
                    end
                end
            end

            cam.PopModelMatrix()
        end
    end)
end
easylua.EndEntity()