<!-- im using the margin gitlab's editor shows for text wrapping ok? -->
# UntitledProject1
~~my first GMOD LUA project :)~~

UntitledProject1 is my long term idea storage of on-going one-off projects.

Why? Because I thought it would be funny to have a huge project look like a 
harmless first project by someone new to a language.

## Purpose

### Map-In-Map
1. **Load times suck.** GMod has horrendous load times (excluding downloads) 
compared to other Source Engine titles, mainly due to the fact of the Lua 
implementation.
2. **Pushing the engine to its limits.** GMod's engine has a raised limit of 
4096 precached models (was 2048 a few years ago) and a raised edict (entity 
dictionary/entity count) limit of 8192. By attemtping to recreate maps in as 
few entities as possible, we can have a comfortable limit of maps to have 
loaded whilst giving players flexibility in a sandbox and giving developers 
flexibility on implementing mini-gamemodes essentially.
3. **User Choice.** Because of how limited Source is compared to other engines 
and games such as Minecraft, and going back to point 1, we don't have the 
luxuary of things such as BungeeCord and "micro-servers" per se. This is where 
BuildLayers plays in to MIM, being able to isolate players into a 
pseudo-sub-space. Multiple gamemodes/minigames running at once, private spaces, 
being able to switch at will without having to wait for things to load.
4. ~~themer didnt end up being my magnum opus~~

### BuildLayers
BuildLayers was inspired by an old GM11 addon that was just called "Layers".
I created it years ago to a "half-assed" state and I think eventually lost the 
code.

So I eventually rewrote it using the old code as a base again, dropped it for a 
couple months/half a year and then picked it up aggain fixing one of its biggest 
issues, map entities. As it was originally created for use in just 
gm_flatgrass, map entities weren't ever rendered between layers.

BuildLayers, as of current, is in a state of needing testing and polish.

## Roadmap

### Map-In-Map
- [ ] Create an easy to use, readable (both efficiently programatically and human) map format
- [ ] Sending map data to the client efficiently (standard library LZMA functions should help here)
- [ ] Store maps on the client
- [ ] Client and server map verification (checksums most likely)
- [ ] Create maps from map data
  - [ ] Ensure we can create maps as one mesh with no issues to reduce entity count
  - [ ] Ensure displacements work well
  - [ ] Bake some model based entities into the mesh (ex: static props)
- [ ] Map rendering
  - [ ] Find an efficient and working way to draw textures without having to polute memory from converting brush based textures via `CreateMaterial`
  - [ ] Optimize performance (culling and the likes)
  - [ ] Model rendering (static props) (should we just use clientside models?)
  - [ ] Lighting
- [ ] Entities
  - [ ] Be able to reimplement most brush and point based entities with litle to no edict usage
- [ ] Physics
  - [ ] Entity collision
  - [ ] Functional entities (doors and the likes)
  - [ ] Ragdoll collision (even client ragdolls (from deaths))
- [ ] Handling certain things serverside
  - [ ] Spawns (just store them, use PlayerSelectSpawn and check the map)
  - [ ] Transitions
  - [ ] Loading/unloading maps on demand
  - [ ] Handling temporary transitions (ex: minigame <--> a certain map for sandbox, deciding where to go if map unloads due to inactivity)
- [ ] Eventual expansion
  - [ ] Voxel maps?
  - [ ] Pseudo-infinite maps?
  - [ ] Live map parsing?
  - [ ] In-game map editor? (screw hammer)

### BuildLayers
- [ ] Add a GUI to the tool menu
- [ ] Test for bugs
- [ ] Move to NetData(?)
- [ ] Add support for other admin mods for the chat commands